import React, { Component } from 'react';
import { Text,View } from 'react-native';

import Home from './src/screens/containers/Home';
import Header from './src/sections/components/Header';
import SuggestionList from './src/videos/containers/SuggestionList';
import CategoryList from './src/videos/containers/CategoryList';
import API from './services/api';
import Player from './src/player/containers/Player';


// const App: () => React$Node = () => {
class App extends Component {
  state = {
    suggestionList: [],
    categoryList: [],
  }
  async componentDidMount() {
    const movies = await API.getSuggestion(10);
    const categories = await API.getMovies();
    this.setState({
      suggestionList: movies,
      categoryList: categories
    })
  }
  render() {
    return (
      // <>
        <Home>
          <Header/>
          <Player />
          <Text>Hola que hace</Text>
          <Text> Buscador </Text>
          <Text> Categorias </Text>
          <CategoryList
            list={this.state.categoryList}
          />
          <SuggestionList
            list = {this.state.suggestionList}
          />
        </Home>
      // </>
    );
  }
};



export default App;
